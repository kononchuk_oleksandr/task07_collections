package com.axeane.collections.menu;

public enum Country {
  UKRAINE ("Ukraine"),
  SPAIN ("Spain"),
  FRANCE ("France"),
  GERMANY ("Germany"),
  POLAND ("Poland"),
  ITALY ("Italy");

  private String title;

  Country(String title) {
    this.title = title;
  }

  public String getTitle() {
    return title;
  }
}
