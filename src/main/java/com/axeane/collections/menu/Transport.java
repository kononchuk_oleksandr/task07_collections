package com.axeane.collections.menu;

public enum Transport {
  BUS ("Bus"),
  TRAIN ("Train"),
  PLANE ("Plane"),
  BOAT ("Boat");

  private String title;

  Transport(String title) {
    this.title = title;
  }

  public String getTitle() {
    return title;
  }
}
