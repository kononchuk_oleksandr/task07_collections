package com.axeane.collections.menu;

import java.util.Scanner;

public class Reader {
  private Scanner input = new Scanner(System.in);

  public String read() {
    return input.nextLine();
  }

}
