package com.axeane.collections.menu;

import java.util.LinkedHashMap;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CountryView {
  private static Logger logger = LogManager.getLogger(CountryView.class);
  private Reader reader = new Reader();
  private TransportView transportView = new TransportView();
  private Map<String, String> menuCountry;
  private Map<String, Printable> menuCountryMethods;

  public CountryView() {
    menuCountry = new LinkedHashMap<>();
    menuCountry.put("1", "1 - " + Country.UKRAINE.getTitle());
    menuCountry.put("2", "2 - " + Country.SPAIN.getTitle());
    menuCountry.put("3", "3 - " + Country.FRANCE.getTitle());
    menuCountry.put("4", "4 - " + Country.GERMANY.getTitle());
    menuCountry.put("5", "5 - " + Country.POLAND.getTitle());
    menuCountry.put("6", "6 - " + Country.ITALY.getTitle());
    menuCountry.put("E", "E - Exit");

    menuCountryMethods = new LinkedHashMap<>();
    menuCountryMethods.put("1", () -> transportView.menu(Country.UKRAINE));
    menuCountryMethods.put("2", () -> transportView.menu(Country.SPAIN));
    menuCountryMethods.put("3", () -> transportView.menu(Country.FRANCE));
    menuCountryMethods.put("4", () -> transportView.menu(Country.GERMANY));
    menuCountryMethods.put("5", () -> transportView.menu(Country.POLAND));
    menuCountryMethods.put("6", () -> transportView.menu(Country.ITALY));
  }

  private void menu() {
    logger.info("\nWelcome to trip application!\n");
    for (String str: menuCountry.values()) {
      logger.info(str);
    }
  }

  public void start() {
    String key = null;
    do {
      menu();
      logger.info("Please select the country:");
      key = reader.read();
      try {
        menuCountryMethods.get(key).print();
      } catch (Exception e) {
        logger.error(e.getMessage());
      }
    } while (!key.equalsIgnoreCase("e"));
  }

}
