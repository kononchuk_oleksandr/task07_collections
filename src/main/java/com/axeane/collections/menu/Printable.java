package com.axeane.collections.menu;

import java.io.IOException;

@FunctionalInterface
public interface Printable {

  void print() throws IOException;

}
