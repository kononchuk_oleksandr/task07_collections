package com.axeane.collections.menu;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TransportView {
  private static Logger logger = LogManager.getLogger(TransportView.class);
  private Reader reader = new Reader();
  private Map<String, String> menuTransport;
  private Map<String, Printable> menuTransportMethods;
  private Country country;

  public TransportView() {
    menuTransport = new LinkedHashMap<>();
    menuTransport.put("1", "1 - " + Transport.BUS.getTitle());
    menuTransport.put("2", "2 - " + Transport.PLANE.getTitle());
    menuTransport.put("3", "3 - " + Transport.BOAT.getTitle());
    menuTransport.put("4", "4 - " + Transport.TRAIN.getTitle());
    menuTransport.put("Q", "Q - Quit");

    menuTransportMethods = new LinkedHashMap<>();
    menuTransportMethods.put("1", () -> printTrip(Transport.BUS));
    menuTransportMethods.put("2", () -> printTrip(Transport.PLANE));
    menuTransportMethods.put("3", () -> printTrip(Transport.BOAT));
    menuTransportMethods.put("4", () -> printTrip(Transport.TRAIN));
  }

  void menu(Country country) throws IOException {
    this.country = country;
    String key = null;
    for (String str: menuTransport.values()) {
      logger.info(str);
    }
    while (true) {
      key = reader.read();
      if (key.equalsIgnoreCase("q")) {
        break;
      } else {
        menuTransportMethods.get(key).print();
      }
    }
  }

  private void printTrip(Transport e) {
    logger.debug("You're going on trip to " + country.getTitle() + "! Take your sit on the " + e.getTitle() + ".");
    System.exit(0);
  }
}
